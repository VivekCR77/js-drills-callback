const { callback1 } = require("./callback1.js");
const { callback2 } = require("./callback2.js");
const { callback3 } = require("./callback3.js");
const { get_json_data } = require("./callback4.js");

const fs = require("fs");

function callback6() {
  const board = get_json_data("data/boards.json");
  const list = get_json_data("data/lists.json");

  for (let key of board) {
    if (key["name"] == "Thanos") {
      callback1(key["id"], (data, error) => {
        if (data) {
          console.log(data);
        } else {
          console.log(error);
        }
      });

      callback2(key["id"], (data, error) => {
        if (data) {
          console.log(data);
        } else {
          console.log(error);
        }
      });

      for (ele of list[key["id"]]) {
        callback3(ele["id"], (data, error) => {
          if (data) {
            console.log(data);
          } else {
            console.log(error);
          }
        });
      }
      break;
    }
  }
}

module.exports = { callback6 };

// callback6();
