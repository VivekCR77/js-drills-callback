const { callback2 } = require("../callback2.js");

callback2("mcu453ed", (data, error) => {
  if (data) {
    console.log(data);
  } else {
    console.log(error);
  }
});
