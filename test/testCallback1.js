const { callback1 } = require("../callback1.js");

callback1("mcu453ed", (data, error) => {
  if (data) {
    console.log(data);
  } else {
    console.log(error);
  }
});
