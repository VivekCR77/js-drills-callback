const { callback3 } = require("../callback3.js");

callback3("qwsa221", (data, error) => {
  if (data) {
    console.log(data);
  } else {
    console.log(error);
  }
});
