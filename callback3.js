const fs = require("fs");

function callback3(card_id, cb) {
  setTimeout(() => {
    const json_data = fs.readFileSync("data/cards.json");
    const data = JSON.parse(json_data.toString());

    for (let key in data) {
      if (key == card_id) {
        var result = data[key];
        break;
      }
    }

    if (result) {
      cb(result, null);
    } else {
      cb(result, "No such ID");
    }
  }, 2 * 1000);
}

module.exports = { callback3 };

// callback3(cb);
