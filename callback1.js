const fs = require("fs");

function callback1(board_id, cb) {
  setTimeout(() => {
    // Your code here
    const json_data = fs.readFileSync("data/boards.json");
    const data = JSON.parse(json_data.toString());

    for (let key of data) {
      if (key["id"] == board_id) {
        var result = key;
        break;
      }
    }

    if (result) {
      cb(result, null);
    } else {
      cb(result, "No such ID");
    }
  }, 2 * 1000);
}

module.exports = { callback1 };

// callback1(cb);
