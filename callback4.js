const { callback1 } = require("./callback1.js");
const { callback2 } = require("./callback2.js");
const { callback3 } = require("./callback3.js");

const fs = require("fs");

function get_json_data(source) {
  const json_data = fs.readFileSync(source);
  const data = JSON.parse(json_data.toString());
  return data;
}

function callback4() {
  const board = get_json_data("data/boards.json");
  const list = get_json_data("data/lists.json");

  for (let key of board) {
    if (key["name"] == "Thanos") {
      callback1(key["id"], (data, error) => {
        if (data) {
          console.log(data);
        } else {
          console.log(error);
        }
      });

      callback2(key["id"], (data, error) => {
        if (data) {
          console.log(data);
        } else {
          console.log(error);
        }
      });

      for (ele of list[key["id"]]) {
        if (ele["name"] == "Mind") {
          callback3(ele["id"], (data, error) => {
            if (data) {
              console.log(data);
            } else {
              console.log(error);
            }
          });
        }
      }

      break;
    }
  }
}

module.exports = { callback4, get_json_data };

// callback4();
