const fs = require("fs");

function callback2(list_id, cb) {
  setTimeout(() => {
    // Your code here
    const json_data = fs.readFileSync("data/lists.json");
    const data = JSON.parse(json_data.toString());

    for (let key in data) {
      if (key == list_id) {
        var result = data[key];
        break;
      }
    }

    if (result) {
      cb(result, null);
    } else {
      cb(result, "No such ID");
    }
  }, 2 * 1000);
}

module.exports = { callback2 };

// callback1(cb);
